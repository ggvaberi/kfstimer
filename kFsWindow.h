#ifndef KFSWINDOW_H
#define KFSWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>
#include <QListWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class kFsWindow; }
QT_END_NAMESPACE

class kFsWindow : public QMainWindow
{
  Q_OBJECT

public:
  kFsWindow(QWidget *parent = nullptr);
  ~kFsWindow();

private slots:
  void on_timPause_clicked();

  void on_timStart_clicked();

  void on_tick();

  void on_wrkTime_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

  void on_rstTime_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

private:
  void work_mode();
  void rest_mode();

private:
  Ui::kFsWindow *ui;
  QTimer timer;

  int fitime[3];
  int wrktime[2];
  int elapse;
  int ctime;
  int ticks;

  bool work;

};
#endif // KFSWINDOW_H
