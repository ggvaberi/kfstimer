#include "kFsWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  kFsWindow w;
  w.show();
  return a.exec();
}
