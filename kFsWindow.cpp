#include "kFsWindow.h"
#include "ui_kFsWindow.h"

kFsWindow::kFsWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::kFsWindow)
{
  ui->setupUi(this);
  connect(&timer, SIGNAL(timeout()), this, SLOT(on_tick()));
  timer.setInterval(1000);

  fitime[0] = 0;
  fitime[1] = 0;
  fitime[2] = 0;

  ui->wrkTime->addItem("1");
  ui->wrkTime->addItem("2");
  ui->wrkTime->addItem("3");
  ui->wrkTime->addItem("5");
  ui->wrkTime->addItem("10");
  ui->wrkTime->setCurrentRow(1);

  ui->rstTime->addItem("1");
  ui->rstTime->addItem("2");
  ui->rstTime->setCurrentRow(0);

  wrktime[0] = 2;
  wrktime[1] = 1;

  elapse = 2;

  ticks = 0;
  ctime = 0;

  work = true;
}

kFsWindow::~kFsWindow()
{
  delete ui;
}

void kFsWindow::work_mode()
{
  elapse = wrktime[0];
  work = true;
  ui->texTime->setStyleSheet("QLabel { color : black; }");
  ctime = ticks;
  QApplication::beep();
}

void kFsWindow::rest_mode()
{
  elapse = wrktime[1];
  work = false;
  ui->texTime->setStyleSheet("QLabel { color : red; }");
  ctime = ticks;
  QApplication::beep();
}

void kFsWindow::on_timPause_clicked()
{
  timer.stop();
}

void kFsWindow::on_timStart_clicked()
{
  timer.start();
}

void kFsWindow::on_tick()
{
  ticks++;

  fitime[0]++;

  if (fitime[0] >= 59) {
    fitime[0] = 0;
    fitime[1]++;
  }

  if (fitime[1] >= 59) {
    fitime[1] = 0;
    fitime[2]++;
  }

  if (fitime[2] > 3) {
    fitime[2] = 0;
  }

  QString text;

  text.sprintf("%02d:%02d:%02d", fitime[2], fitime[1], fitime[0]);
  ui->texTime->setText(text);

  int e = ticks - ctime;

  if ((e) >= (elapse * 60))  {
    if (work)
      rest_mode();
    else
      work_mode();
  }
}

void kFsWindow::on_wrkTime_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
  wrktime[0] = atoi(current->text().toLocal8Bit().data());

  if (work)
    elapse = wrktime[0];
}

void kFsWindow::on_rstTime_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
  wrktime[1] = atoi(current->text().toLocal8Bit().data());
  if (!work)
    elapse = wrktime[1];
  //pLabel->setStyleSheet("QLabel { background-color : red; color : blue; }");
}
